// import file path library
const path = require('path')

// Find the system root directory
const getRootDir = path.parse(process.cwd()).root

// import http and https libraries
const http = require('http');
const https = require('https');

//  import fs library to access local file system
const fs = require('fs');

// Access OS functionalities using the child_process
const { exec } = require('child_process');

// Use the process.platform property to access type of OS in Nodejs
function getOS(){
  console.log(process.platform);
}

// Using the exec method, pass the command for installing any software as an argument for Linux machines
function installLinuxSoftware(insertCommand){
  exec(insertCommand, (err, stdout, stderr) => {
    if (err) {
      // node couldn't execute the command
      return;
    }
    // the *entire* stdout and stderr (buffered)
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
}

// Initialize and instantiate a new variable that would serve as the new folder to be created
const dirName = "/software";

// Use fs.mkdir method to create a new directory to the path provided (the variable dirName)
const newDir = fs.mkdir(dirName, (err) => {
  if (err) {
      return;
  }
});


/**
 * Download softwares via url for windows and macos using http.get request, 
  then drop file in local system by reading a location using fs.createWriteStream method
 *  */ 
function downloadSoftwareForWindowsAndMac(url, fileLocation, httpProtocol){
    httpProtocol.get(url, resp => resp.pipe(fs.createWriteStream(fileLocation)));
}

/**
 * Install softwares(wget, curl and Nodejs) on the different platforms
 * The -y flag prevents any hookups from yes/no prompts
 */
//  Install Wget into a Linux machine
function installWgetInLinux(){
    installLinuxSoftware('sudo apt-get install wget -y');
}

// Install Wget into a Windows machine
function installWgetInWindows(){
  downloadSoftwareForWindowsAndMac("http://ftp.gnu.org/gnu/wget/wget-1.11.4.tar.gz", getRootDir + "/Users" + newDir + "/wget-1.11.4.tar.gz", http);
}

// Install Wget into MacOS
function installWgetInMacOs(){
  downloadSoftwareForWindowsAndMac("http://ftp.gnu.org/gnu/wget/wget-1.11.4.tar.gz", getRootDir + "/Users" + newDir + "/wget-1.11.4.tar.gz", http);
}

//  Install Curl into a Linux machine
function installCurlInLinux(){
  installLinuxSoftware('sudo apt-get install curl -y');
}

// Install Curl into a Windows machine
function installCurlInWindows(){
  downloadSoftwareForWindowsAndMac("https://curl.se/windows/dl-7.74.0_2/curl-7.74.0_2-win32-mingw.zip", getRootDir + "/Users" + newDir + "/curl-7.74.0_2-win32-mingw.zip", https);
}

// Install Curl into MacOS
function installCurlInMacOs(){
  downloadSoftwareForWindowsAndMac("https://curl.haxx.se/download/curl-7.74.0.tar.gz", getRootDir + "/Users" + newDir + "/curl-7.74.0.tar.gz", https)
}

//  Install Nodejs into a Linux machine
function installNodeInLinux(){
  installLinuxSoftware('sudo apt-get install nodejs -y')
}

 // Install Nodejs into a Windows machine
function installNodeInWindows(){
  downloadSoftwareForWindowsAndMac("https://nodejs.org/dist/v14.15.4/node-v14.15.4.tar.gz", getRootDir + "/Users" + newDir + "/node-v14.15.4.tar.gz", https);
}

// Install Nodejs into MacOS
function installNodeInMacOs(){
  downloadSoftwareForWindowsAndMac("https://nodejs.org/dist/v14.15.4/node-v14.15.4.tar.gz", getRootDir + "/Users" + newDir + "/node-v14.15.4.tar.gz", https);
}

// Check for type of software version requested and the os type and install softwares accordingly
function installSoftwareForOS(versionCommand){
  // Check linux, windows and macos for wget
  if(versionCommand == "wget --version" && process.platform == "linux"){
    installWgetInLinux();
  }else if(versionCommand == "wget --version" && process.platform == "win32"){
    installWgetInWindows();
  }else if(versionCommand == "wget --version" && process.platform == "darwin"){
    installWgetInMacOs();
  }

  // Check linux, windows and macos for curl
  if(versionCommand == "curl --version" && process.platform == "linux"){
    installCurlInLinux();
  }else if(versionCommand == "curl --version" && process.platform == "win32"){
    installCurlInWindows();
  }else if(versionCommand == "curl --version" && process.platform == "darwin"){
    installCurlInMacOs();
  }

  // Check linux, windows and macos for node
  if(versionCommand == "node --version" && process.platform == "linux"){
    installNodeInLinux();
  }else if(versionCommand == "node --version" && process.platform == "win32"){
    installNodeInWindows();
  }else if(versionCommand == "node --version" && process.platform == "darwin"){
    installNodeInMacOs();
  }
}

/**
 * Check if any of the softwares have been installed. 
 * if installed, signal that software has been installed, else, install software
 *  */ 
function checkIfSoftwareIsInstalled(version){
  exec(version, (err, stdout) => {

    // Get the first word in the command for a version which is the name of software
    function getFirstWordInCommand(str) {
      let spaceIndex = str.indexOf(' ');
      return spaceIndex === -1 ? str : str.substr(0, spaceIndex);
    };

    // If software is not found, print that command not found and...
    if (err) {
      console.log(getFirstWordInCommand(version) + " command not found");
      console.log("Installing " + getFirstWordInCommand(version) + "...");
    }

    // If software exists, print out every information on the software
    if(stdout != ""){
      console.log(`${stdout}`)
    }else{
      // else if software does not exist, install the software after getting the type of software to install
      installSoftwareForOS(version)
    }
    
  });
}

getOS();
checkIfSoftwareIsInstalled("node --version")
checkIfSoftwareIsInstalled("wget --version")
checkIfSoftwareIsInstalled("curl --version")